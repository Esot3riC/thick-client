#### Tools for reversing .NET application
1. [dnSpy](https://github.com/dnSpy/dnSpy): [Download](https://github.com/dnSpy/dnSpy/releases):- Use for Disassembling , getting C-Sharp code, getting IL Code & Patching the code. most imp debugging application and look at variables/function at run time.(Folder6 Video4) To edit the application and make changes in application to work accordingly for e.g. enable/disable buttons. dnSpy is a debugger and .NET assembly editor. You can use it to edit and debug assemblies even if you don't have any source code available. It **automaticall decompile the code** for us. (Folder2 Video5 MTCAPT) 
2. [DotPeek](https://www.jetbrains.com/decompiler/download/): (Folder6 Video2)
3. ILSpy & Reflexil: use for patching application (Folder6 Video5,6)
4. ILASM (intermediate language assembler) (Come with .NET framework):(Folder6 Video7)
5. ILDASM (intermediate language Disassembler) (Come with Visual Studio):
