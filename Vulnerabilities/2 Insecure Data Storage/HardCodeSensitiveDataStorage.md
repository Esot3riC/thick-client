## Hard Code Data storage (Credentials, sessions & Sensitive data):
Some time developer leave/store hardcode Sensitive details in thick Client app. Tools use for this:
1. Strings(Sys Internals): 
- CMD: D:\Testing tools\Thick Client testing tools\SysinternalsSuite>strings.exe C:\Users\Waseem\Desktop\CTMThickClientTesting\CTM\CTM.exe(testing file path-no space) > C:\Users\Waseem\Desktop\CTMThickClientTesting\CTM\result.txt

2. [dnSpy](https://github.com/dnSpy/dnSpy): [Download](https://github.com/dnSpy/dnSpy/releases):- Load the .exe file, Decompile code and check for values and logic found using Strings   (Folder5 Video1 MTCAPT)

