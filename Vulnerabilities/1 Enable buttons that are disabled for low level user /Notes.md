- some time user loges in and some menu are diabled for him for e.g. reset other's password etc.
- developer disable these types of menu in client and think on oce can enable it 


#### Steps:- 
1. [dnSpy](https://github.com/dnSpy/dnSpy): [Download](https://github.com/dnSpy/dnSpy/releases):- To edit the application and make changes in application to work accordingly for e.g. enable/disable buttons. dnSpy is a debugger and .NET assembly editor. You can use it to edit and debug assemblies even if you don't have any source code available. It **automaticall decompile the code** for us. (reference:- Folder2 Video5 MTCAPT)
2. open the testing application in DnSpy application File >> select .exe/application file 
3. Find the button we want to enable. 
4. Check screen/Activity name where the button is present
5. check for button name and it's logic 
6. edit the code accordingly
7. save the code and start it. 
