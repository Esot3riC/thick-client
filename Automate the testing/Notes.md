#### We need 2 tools for automate the testing:
1. [DotPeek](https://www.jetbrains.com/decompiler/download/)
2. [Visual Code Grepper](https://sourceforge.net/projects/visualcodegrepp/)

#### How to scan the code:
1. Dotpeek: first use it to decompile the target file and then give decompiled file to scanner.
2. Steps:- file >> open >> (right click)select the file >> Export to project >> select destination folder >> Export.
3. Visual Code Grepper: automate the testing
4. Steps:- file >> open exported file >> select language(seetings) >> Scan >> Full scan
5. check the result (Folder7 video3)    
