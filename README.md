# Thick Client 

- thick client application build on **C-sharp**, **Dot Net** , **C/C++**, **Java applets**   

![](/Images/thickclient_Artictuire.png)

# Important Links
- https://www.cyberark.com/resources/threat-research-blog/thick-client-penetration-testing-methodology
- https://www.netspi.com/blog/technical/thick-application-penetration-testing/introduction-to-hacking-thick-clients-part-4-the-assemblies/
- https://www.kitploit.com/2021/03/retoolkit-reverse-engineers-toolkit.html
- https://github.com/Hari-prasaanth/Thick-Client-Pentest-Checklist
- https://hariprasaanth.notion.site/hariprasaanth/THICK-CLIENT-PENTESTING-CHECKLIST-35c6803f26eb4c9d89ba7f5fdc901fb0 

## Tools
- CFF Explorer
- Sysinternal Suite
- Wireshark
- ProcMon (Process moniter - Sysinternal): help us to understand file system changes. What is bieng accessed by the application (file create, Password store in registry etc.) **_Select "Process Name" if you are choosing application name in ProcMon filter.**
- "MITM-Relay + Burp Suite" or "Echo Mirage" (Intercept TCP Traffic) (Folder4 Video2 MTCAPT)
- Burp Suite (Intercept HTTP Traffic)
- Strings(Sys Internals): for checing hardcoded stored Senstitve data in .exe file.  CMD: D:\Testing tools\Thick Client testing tools\SysinternalsSuite>strings.exe C:\Users\Waseem\Desktop\CTMThickClientTesting\CTM\CTM.exe(testing file path-no space) > C:\Users\Waseem\Desktop\CTMThickClientTesting\CTM\result.txt
- [RegShot](https://sourceforge.net/projects/regshot/):- tell the changes done in registry after running the application. 1st take screenshot before running the test application then run the test application and take 2nd screenshot. click on compare at the end. will get HTML page. (folder5 Video2)
- [Process Hacker tool](https://processhacker.sourceforge.io/downloads.php): tool to find database connecton String in memory. (folder5 Video3)
- [Visual Code Grepper](https://sourceforge.net/projects/visualcodegrepp/): To automate the code scanning.
- sigcheck.exe (Sysinternals tool):- this is used to test binary is signed or not. If not report this as an issue.
- [BinScope](https://www.microsoft.com/en-us/download/confirmation.aspx?id=44995):- this tools Check Binary is complied using Appropriate security option in compiler.

#### Tools for reversing .NET application
1. [dnSpy](https://github.com/dnSpy/dnSpy): [Download](https://github.com/dnSpy/dnSpy/releases):- Use for Disassembling , getting C-Sharp code, getting IL Code & Patching the code. most imp debugging application and look at variables/function at run time. To edit the application and make changes in application to work accordingly for e.g. enable/disable buttons. dnSpy is a debugger and .NET assembly editor. You can use it to edit and debug assemblies even if you don't have any source code available. It **automaticall decompile the code** for us. (Folder2 Video5 MTCAPT) 
2. [DotPeek](https://www.jetbrains.com/decompiler/download/): decompile the code. which is used for automation. 
3. ILSpy & Reflexil: use for patching application
4. ILASM (intermediate language assembler) (Come with .NET framework):
5. ILDASM (intermediate language Disassembler) (Come with Visual Studio):

# things to check
- explore functanality/features of application. what king of data it is handling 
- kind of n/w comminucation it is making (HTTP/TCP). Tools for it.
> - Sysinternal > TCPView (also check Destination IP )
> - Wireshark (check network traffic also)
- Is this a 2-tier or 3-tier application ( )
- Development language of the application along with the SDK versions. Tools for it.
> - CFF Explorer (**C, C++** = check for bufferoverflow // **C-Sharp, .NET** = Easy to decompile and reverse engenere it. and **Microsoft Silverlight**)
- what files are accessed by the application. (ProcMon)
- what changes it is making to the file sysytem. (ProcMon)
> - ProcMon (Process moniter - Sysinternal)
- Install the application and observe the following details:
1. End user license agreement is displayed during the installation.
1. Use of third components, libraries and frameworks.
1. Check for intresting files in application directories
- Observe the effectiveness of serial keys if they are used to activate the application. Look for:
1. Randomness
1. Weaknesses in serial keys
-Verify the file properties of installer/setup and check for code signing.
-Check for validity and authenticity of the code signature authority/certificate.
- Check the following test cases on all the files in the installation directory:
1. File name or Internal Name: The actual file name of the binary using which it was compiled.
1. File Version: The binary should contain the actual build version of the application.
1. Company name, Copyright information and legal trademarks,
1. Validity of code signature.
1. validity of code signing certificate.
- Verify the following checks in the decompiled code:
1. Check for "IsDebuggerPresent"
1. Check for "IsDebuggerPresent Using the PEB"
1. Check for "CheckRemoteDebuggerPresent"
1. Check for "NtQueryInformationProcess"
- Load the application within a debugger and verify that the application should not execute.
- Check for use of packers or compressors as an extra layer of protection. Common packers such as UPX, UPack, ASPack, APProtect should not be used.
- DEP, ASLR should be enabled on all the code files build on windows platform (exe, dll's, ocx etc.) (https://github.com/NetSPI/PESecurity)
- SAFESEH should be ON for all the 32 bit files build on windows platform (exe, dll's, ocx etc.)
- Check for the contents of .XML, .log or any other configuration files so that it doesn’t contain any sensitive information or any sensitive code flow.
- Verify the temp folder for any sensitive file which might be created during execution of the application.
- Check for any sensitive information being stored in the registry keys of the machine.
- Perform a memory dump of the application and check for any credentials, PCI or PII information stored in them both before and after logout.
- Disassemble the binary to obtain the source code of the application and check for any sensitive information, workflows, classes, methods etc. Use tools like .net reflector, iLSpy, JD GUI etc.
- Check for non-standard or custom cryptographic routines or hardcoded secrets like encryption keys, credentials, server resources, path to server etc.
- Check for vulnerable API's which might be used in the source code of the application.
- Perform a string dump of the application and check for any sensitive information.
- Check for DLL injection.

